{
  inputs = {
    devshell.url = "github:numtide/devshell";
    flake-parts.url = "github:hercules-ci/flake-parts";
    incl.url = "github:divnix/incl";
    nixpkgs.url = "github:nixos/nixpkgs";
    systems.url = "github:nix-systems/default";
  };

  outputs = inputs:
    inputs.flake-parts.lib.mkFlake { inherit inputs; } {
      imports = [
        inputs.flake-parts.flakeModules.easyOverlay
        inputs.devshell.flakeModule
      ];
      systems = import inputs.systems;
      perSystem = { lib, pkgs, config, ... }: {

        formatter = pkgs.nixpkgs-fmt;

        packages = {

          frontend-package = pkgs.stdenv.mkDerivation {
            pname = "frontend";
            version = inputs.self.lastModifiedDate;
            src = inputs.incl ./frontend [ ./frontend ];
            buildInputs = [ pkgs.hugo ];
            buildPhase = ''
              hugo
            '';
            installPhase = ''
              mkdir -p $out
              cp -r public/. $out/
            '';
          };

          frontend-deploy = pkgs.writeShellScriptBin "deploy" ''
            ${./frontend/deploy-netlify.sh} "$@" ${
              config.packages.frontend-package
            }
          '';


          backend-indieauth-package = pkgs.haskellPackages.callCabal2nix "backend-indieauth"
            (inputs.incl ./backend-indieauth [ ./backend-indieauth ])
            { };

          backend-indieauth-image = pkgs.dockerTools.buildLayeredImage {
            name = "backend-indieauth";
            contents = [ pkgs.bash config.packages.backend-indieauth-package ];
            config.packages.Cmd = [ "/bin/backend-indieauth" ];
          };

          backend-indieauth-deploy = pkgs.writeShellScriptBin "deploy"
            (
              let img = config.packages.backend-indieauth-image;
              in ''
                set -euxo pipefail
                docker load -i ${img}
                docker tag ${img.imageName}:${img.imageTag} codeberg.org/martini/blog-backend-indieauth
                docker push codeberg.org/martini/blog-backend-indieauth
              ''
            );

        };

        devshells.default = {
          imports = [ "${inputs.devshell.outPath}/extra/locale.nix" ];

          name = "jarkad.net.eu.org";

          extra.locale.lang = "en_US.UTF-8";

          packages = [
            (pkgs.ghc.withPackages (ps: config.packages.backend-indieauth-package.getBuildInputs.haskellBuildInputs))
          ];

          commands =
            lib.concatLists
              (lib.mapAttrsToList
                (category:
                  map (package:
                    { inherit package category; }))
                {
                  "backend commands" = [
                    pkgs.cabal-install
                    pkgs.ghcid
                    pkgs.haskellPackages.hlint
                    pkgs.haskellPackages.hpack
                  ];
                  "frontend commands" = [
                    pkgs.hugo
                    pkgs.overmind
                    pkgs.yarn
                  ];
                  "general commands" = [
                    pkgs.caddy
                    pkgs.overmind
                    pkgs.python3
                  ];
                });

        };
      };
    };
}
