{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -Wno-orphans #-}

module Uri (renderURI, parseURI, urlEncode, urlDecode, URI) where

import Network.URI (escapeURIString, isUnescapedInURI, parseAbsoluteURI, unEscapeString, uriToString)
import Data.Aeson
import Servant.API

instance FromJSON URI where
  parseJSON =
    withText "URI" $
      maybe (fail "Invalid URI") pure . parseURI

instance ToJSON URI where
  toJSON = toJSON . renderURI

instance FromHttpApiData URI where
  parseUrlPiece = maybeToRight "Invalid URI" . parseURI . urlDecode

instance ToHttpApiData URI where
  toUrlPiece = urlEncode . renderURI

renderURI :: URI -> Text
renderURI uri' = toText $ uriToString id uri' ""

parseURI :: Text -> Maybe URI
parseURI = parseAbsoluteURI . toString

urlEncode :: Text -> Text
urlEncode = toText . escapeURIString isUnescapedInURI . toString

urlDecode :: Text -> Text
urlDecode = toText . unEscapeString . toString
