{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeApplications #-}

module Handlers.Validate (handle) where

import App
import Routes
import Signed
import Util

import Servant
import Data.Time

handle :: ValidationRequest -> AppM (Union '[WithStatus 200 Profile, ValidationException])
handle req' = do
  conf <- ask
  time <- liftIO getCurrentTime

  -- TODO: logging
  putStrLn $ toJson $ object [ "validation_request" .= req' ]

  case unsign conf (req' ^. #code) of
    Left err ->
      respond $ InvalidAuthCode $ "Invalid code: " <> err
    Right auth_code
      | (auth_code ^. #expiration_time) < time ->
        respond $ ExpiredAuthCode $ mconcat
          [ "Code expired at "
          , show (auth_code ^. #expiration_time)
          , "; current server time: "
          , show time
          ]
      | (auth_code ^. #redirect_uri) /= (req' ^. #redirect_uri) ->
        respond RedirectUriDoesNotMatch
      | (auth_code ^. #client_id) /= (req' ^. #client_id) ->
        respond CliendIdDoesNotMatch
      | otherwise ->
        respond $ WithStatus @200 Profile
          { me = conf ^. #user_domain
          }

