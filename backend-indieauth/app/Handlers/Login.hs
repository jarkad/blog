{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE OverloadedStrings #-}

module Handlers.Login (handle) where

import App
import CSRF
import HTML
import Link
import Routes
import Uri
import Util

import Text.Blaze.Html5 ((!))
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A

handle :: URI -> Text -> URI -> Text -> Text -> Bool -> AppM H.Html
handle me' client_id' redirect_uri' state' response_type' invalid' = do
  conf <- ask
  let csrf_token' = makeCSRF conf client_id' redirect_uri' state'

  -- TODO: logging
  putStrLn $ toJson $ object
    [ "login_request" .= object
      [ "me" .= me'
      , "client_id" .= client_id'
      , "redirect_uri" .= redirect_uri'
      , "state" .= state'
      , "response_type" .= response_type'
      , "invalid" .= invalid'
      ]
    ]

  pure do
    page "Login" do
      H.h1 "Login"
      H.p do
        "You are attempting to login with client "
        H.code do
          H.toMarkup client_id'
        "."
      H.p do
        "After login you will be redirected to "
        H.code do
          H.toMarkup $ renderURI redirect_uri'
        "."
      H.form ! A.class_ "mha" ! A.method "POST" ! A.action (H.textValue (links ^. #auth.to (renderLink conf))) $ do
        H.input ! A.type_ "hidden" ! A.name "csrf_token"    ! A.value (H.textValue (toUrlPiece csrf_token'))
        H.input ! A.type_ "hidden" ! A.name "me"            ! A.value (H.textValue (renderURI me'))
        H.input ! A.type_ "hidden" ! A.name "client_id"     ! A.value (H.textValue client_id')
        H.input ! A.type_ "hidden" ! A.name "redirect_uri"  ! A.value (H.textValue (renderURI redirect_uri'))
        H.input ! A.type_ "hidden" ! A.name "state"         ! A.value (H.textValue state')
        H.input ! A.type_ "hidden" ! A.name "response_type" ! A.value (H.textValue response_type')
        H.table do
          H.tr do
            H.th do
              H.label ! A.for "user" $ "Username"
            H.td do
              H.input ! A.id "user" ! A.disabled "" ! A.value (conf ^. #user_domain . to renderURI . to H.textValue)
          H.tr do
            H.th do
              H.label ! A.for "pass" $ "Password"
            H.td do
              H.input ! A.id "pass" ! A.type_ "password" ! A.name "password"
          when invalid' do
            H.tr do
              H.td ""
              H.td ! A.style "background:red;color:white" $ "Invalid password."
          H.tr do
            H.td ""
            H.td do
              H.button "Log in"
