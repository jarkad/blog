{-# LANGUAGE BlockArguments    #-}
{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE OverloadedLabels  #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications  #-}
{-# LANGUAGE TypeFamilies      #-}

module Handlers.Auth
  ( AuthRequest (..)
  , AuthException (..)
  , handle
  ) where

import App
import AuthCode
import Redirect
import Routes
import Signed
import Util

import Crypto.Hash (SHA256)
import Data.Time
import Network.URI.Lens (uriQueryLens)
import Servant

handle :: AuthRequest -> AppM (Union '[Redirect 302, AuthException])
handle req' = do
  conf <- ask

  whenLeft (unsign conf (req' ^. #csrf_token)) \err ->
    throwError err401
      { errBody =
          "Invalid CSRF token: " <> encodeUtf8 err
      }

  -- TODO: logging
  putStrLn $ toJson $ object [ "auth_request" .= req' ]

  if show (hash @ByteString @SHA256 (req' ^. #password . to encodeUtf8)) == conf ^. #user_hash
  then do
    time <- liftIO getCurrentTime
    let code' = makeAuthCode conf time (req' ^. #client_id) (req' ^. #redirect_uri) []
    respond $ redirectUri @302 $
      req' ^. #redirect_uri
        & uriQueryLens %~
          (\q -> q <> (if q == "" then "?" else "&")
                   <> "code="   <> toString (toQueryParam code')
                   <> "&state=" <> toString (req' ^. #state . to toQueryParam))
  else
    respond $ redirectLink @302 conf $ (links ^. #login)
      (req' ^. #me)
      (req' ^. #client_id)
      (req' ^. #redirect_uri)
      (req' ^. #state)
      (req' ^. #response_type)
      True -- invalid
