module App
  ( AppM
  ) where

import Config
import Servant (Handler)

type AppM = ReaderT Config Handler
