{-# LANGUAGE DeriveGeneric    #-}
{-# LANGUAGE OverloadedLabels #-}

module AuthCode
  ( AuthCode (expiration_time, scope, client_id, redirect_uri)
  , makeAuthCode
  ) where

import Config
import Signed
import Uri
import Util

import Data.Time (UTCTime, addUTCTime)

data AuthCode = AuthCode
  { client_id :: Text
  , expiration_time :: UTCTime
  , redirect_uri :: URI
  , scope :: [Text]
  } deriving (Eq, Show, Generic)

instance FromJSON AuthCode
instance ToJSON AuthCode

instance FromHttpApiData AuthCode where
  parseUrlPiece = fromJson <=< fromBase64

instance ToHttpApiData AuthCode where
  toUrlPiece = toBase64 . toJson

makeAuthCode :: Config -> UTCTime -> Text -> URI -> [Text] -> Signed AuthCode
makeAuthCode conf time _client_id _redirect_uri _scope = do
  let _expiration_time = addUTCTime (conf ^. #auth_code_ttl) time
  let auth_code = AuthCode
        { client_id       = _client_id
        , expiration_time = _expiration_time
        , redirect_uri    = _redirect_uri
        , scope           = _scope
        }
  sign conf auth_code
