{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE DeriveGeneric #-}

module Routes
  ( Routes (..)
  , Profile (..)
  , LoginRequest (..)
  , AuthRequest (..)
  , AuthException (..)
  , ValidationRequest (..)
  , ValidationException (..)
  , links
  ) where

import AuthCode
import CSRF
import Redirect
import Signed
import Uri
import Util

import qualified Example.Routes

import Servant
import Servant.API.Generic
import Servant.HTML.Blaze
import Web.FormUrlEncoded

data Routes route = Routes
  { login    :: route :- "login"
                      :> QueryParam' '[Required, Strict] "me"            URI
                      :> QueryParam' '[Required, Strict] "client_id"     Text
                      :> QueryParam' '[Required, Strict] "redirect_uri"  URI
                      :> QueryParam' '[Required, Strict] "state"         Text
                      :> QueryParam' '[Required, Strict] "response_type" Text
                      :> QueryFlag "invalid"
                      :> Get '[HTML] Html
  , auth     :: route :- "auth"
                      :> ReqBody '[FormUrlEncoded] AuthRequest
                      :> UVerb 'POST '[JSON] '[Redirect 302, AuthException]
  , validate :: route :- "validate"
                      :> ReqBody '[FormUrlEncoded] ValidationRequest
                      :> UVerb 'POST '[JSON] '[WithStatus 200 Profile, ValidationException]
  , example  :: route :- "example"
                      :> NamedRoutes Example.Routes.Routes
  } deriving (Generic)

links :: Routes (AsLink Link)
links = allFieldLinks


{- HLINT ignore "Use newtype instead of data" -}
data Profile = Profile
  { me :: URI
  } deriving (Eq, Show, Generic)

instance FromJSON Profile
instance ToJSON Profile


data LoginRequest = LoginRequest
  { _me :: URI
  , _client_id :: Text
  , _redirect_uri :: URI
  , _state :: Text
  , _response_type :: Text
  } deriving (Eq, Show)


data AuthRequest = AuthRequest
  { me :: URI
  , client_id :: Text
  , redirect_uri :: URI
  , state :: Text
  , response_type :: Text
  , csrf_token :: CSRFToken
  , password :: Text
  } deriving (Eq, Show, Generic)

instance FromForm AuthRequest
instance ToJSON AuthRequest


data AuthException
  = InvalidRedirectUri
  | RedirectUriNotAbsolute
  deriving (Show, Typeable, Generic)

instance Exception AuthException
instance FromJSON AuthException
instance ToJSON AuthException

instance HasStatus AuthException where
  type StatusOf AuthException = 400


data ValidationRequest = ValidationRequest
  { code :: Signed AuthCode
  , client_id :: Text
  , redirect_uri :: URI
  } deriving (Eq, Show, Generic)

instance FromForm ValidationRequest
instance ToForm ValidationRequest

instance FromJSON ValidationRequest
instance ToJSON ValidationRequest


data ValidationException
  = InvalidAuthCode Text
  | ExpiredAuthCode Text
  | RedirectUriDoesNotMatch
  | CliendIdDoesNotMatch
  deriving (Show, Typeable, Generic)

instance Exception ValidationException
instance FromJSON ValidationException
instance ToJSON ValidationException

instance HasStatus ValidationException where
  type StatusOf ValidationException = 400
