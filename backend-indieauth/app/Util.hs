{-# LANGUAGE BlockArguments    #-}
{-# LANGUAGE TypeApplications  #-}

module Util
  ( toJson
  , fromJson
  , FromJSON (..)
  , ToJSON (..)
  , object
  , (.=)
  , hmac
  , FromHttpApiData (..)
  , ToHttpApiData (..)
  , to
  , hash
  , toBase64
  , fromBase64
  , Html
  )
  where

import Crypto.Hash (SHA256, hash)
import Data.Aeson
import Data.ByteString.Base64.URL (encodeBase64, decodeBase64)
import Data.Generics.Labels ()
import Lens.Micro (to)
import Servant (FromHttpApiData(..), ToHttpApiData(..))
import Text.Blaze.Html5 (Html)
import qualified Crypto.MAC.HMAC as HMAC

toBase64 :: Text -> Text
toBase64 = encodeBase64 . encodeUtf8

fromBase64 :: Text -> Either Text Text
fromBase64 = fmap decodeUtf8 . decodeBase64 . encodeUtf8

toJson :: ToJSON a => a -> Text
toJson = decodeUtf8 . encode

fromJson :: FromJSON a => Text -> Either Text a
fromJson = first toText . eitherDecode . encodeUtf8

hmac :: IsString a => ByteString -> ByteString -> a
hmac key message = show $ HMAC.hmacGetDigest $ HMAC.hmac @_ @_ @SHA256 key message
