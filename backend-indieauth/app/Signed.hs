{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE OverloadedStrings #-}

module Signed (Signed, sign, unsign) where

import Config
import Util

data Signed a = Signed
  { content :: a
  , signature :: Text
  } deriving (Eq, Read, Show, Generic)

instance FromJSON a => FromJSON (Signed a)
instance ToJSON a => ToJSON (Signed a)

instance FromJSON a => FromHttpApiData (Signed a) where
  parseUrlPiece = fromJson

instance ToJSON a => ToHttpApiData (Signed a) where
  toUrlPiece = toJson

sign :: ToJSON a => Config -> a -> Signed a
sign conf message = Signed
  { content     =
      message
  , signature =
      hmac (conf^. #app_key . to encodeUtf8)
           (message ^. to toJson . to encodeUtf8)
  }

unsign :: ToJSON a => Config -> Signed a -> Either Text a
unsign conf signed
  | hmac (conf^. #app_key . to encodeUtf8) (signed ^. #content . to toJson . to encodeUtf8) == (signed ^. #signature) =
    Right (signed ^. #content)
  | otherwise =
    Left "invalid signature"

