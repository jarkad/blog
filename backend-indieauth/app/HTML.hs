{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE OverloadedStrings #-}

module HTML (page) where

import Text.Blaze.Html5 ((!))
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A

page :: H.Markup -> H.Html -> H.Html
page title body = do
  H.docTypeHtml do
    H.head do
      H.meta ! A.charset "utf-8"
      H.meta ! A.name "viewport" ! A.content "width=device-width, initial-scale=1.0"
      H.title title
      H.link ! A.rel "stylesheet" ! A.href "/style.css"
    H.body body
