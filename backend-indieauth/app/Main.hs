{-# LANGUAGE BangPatterns          #-}
{-# LANGUAGE BlockArguments        #-}
{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedLabels      #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE TypeFamilies          #-}


module Main (main) where

import App
import Config
import Routes
import Util
import qualified Example.Handlers.Begin
import qualified Example.Handlers.Callback
import qualified Example.Routes as Example
import qualified Handlers.Auth
import qualified Handlers.Login
import qualified Handlers.Validate

import Servant
import Servant.Server.Generic (AsServerT, genericServeT)
import qualified Network.Wai.Handler.Warp as Warp

server :: Routes (AsServerT AppM)
server = Routes
  { login = Handlers.Login.handle
  , auth = Handlers.Auth.handle
  , validate = Handlers.Validate.handle
  , example = Example.Routes
    { begin = Example.Handlers.Begin.handle
    , callback = Example.Handlers.Callback.handle
    }
  }

app :: Config -> Application
app conf = genericServeT (`runReaderT` conf) server

main :: IO ()
main = do
  -- Forcibly evaluate config to crash early
  !conf <- lookupConfig
  putStrLn $ toJson $ object [ "config" .= conf ]
  putTextLn $ "Running on port " <> (conf ^. #listen_port . to show)
  Warp.run (conf ^. #listen_port) (app conf)
