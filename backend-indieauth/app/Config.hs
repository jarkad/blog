{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}

module Config
  ( Config(..)
  , ConfigException(..)
  , lookupConfig
  ) where

import Uri

import Control.Exception (throw)
import Data.Aeson (ToJSON)
import Data.Time (NominalDiffTime)
import System.Environment (lookupEnv)

-- | All fields should be strict to fail early if some env vars are not set.
data Config = Config
  { app_url       :: !URI
  , app_key       :: !Text
  , user_domain   :: !URI
  , user_hash     :: !Text
  , listen_port   :: !Int
  , auth_code_ttl :: !NominalDiffTime
  } deriving (Show, Generic)

instance ToJSON Config

data ConfigException
  = NoEnvVar
    { env_var     :: String
    }
  | InvalidIntException
    { env_var     :: String
    , explanation :: Text
    }
  | InvalidUrlException
    { env_var     :: String
    }
  deriving (Show, Typeable, Generic)

instance Exception ConfigException
instance ToJSON ConfigException

lookupConfig :: IO Config
lookupConfig = do
  _app_url        <- lookupEnvURL             "APP_URL"
  _app_key        <- lookupEnvText            "APP_KEY"
  _user_domain    <- lookupEnvURL             "USER_DOMAIN"
  _user_hash      <- lookupEnvText            "USER_HASH"
  _listen_port    <- lookupEnvRead            "PORT"
  _auth_code_ttl  <- lookupEnvNominalDiffTime "AUTH_CODE_TTL"
  let conf = Config
        <$> _app_url 
        <*> _app_key 
        <*> _user_domain 
        <*> _user_hash 
        <*> _listen_port
        <*> (_auth_code_ttl <> Right 7200)
  either throw pure conf

lookupEnvText :: String -> IO (Either ConfigException Text)
lookupEnvText var =
  fmap toText . maybeToRight (NoEnvVar var) <$> lookupEnv var

parse :: Read a => String -> Text -> Either ConfigException a
parse var env = do
  case readEither env of
    Left err ->
      Left $ InvalidIntException var $ unwords [err, toText var, env]
    Right port ->
      Right port

lookupEnvRead :: Read a => String -> IO (Either ConfigException a)
lookupEnvRead var = do
  env <- lookupEnvText var
  pure $ env >>= parse var

lookupEnvURL :: String -> IO (Either ConfigException URI)
lookupEnvURL var =
  (>>= maybeToRight (InvalidUrlException var) . parseURI) <$> lookupEnvText var

lookupEnvNominalDiffTime :: String -> IO (Either ConfigException NominalDiffTime)
lookupEnvNominalDiffTime var =
  second fromInteger <$> lookupEnvRead var
