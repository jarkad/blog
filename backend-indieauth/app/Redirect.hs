{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE DataKinds #-}

module Redirect
  ( Redirect
  , redirect
  , redirectUri
  , redirectLink
  ) where

import Config
import Link
import Uri

import Servant

type Redirect (code :: Nat) = WithStatus code (Headers '[Header "Location" Text] NoContent)

redirect :: Text -> Redirect (code :: Nat)
redirect loc = WithStatus $ addHeader loc NoContent

redirectUri :: URI -> Redirect (code :: Nat)
redirectUri = redirect . renderURI

redirectLink :: Config -> Link -> Redirect (code :: Nat)
redirectLink = (redirect .) . renderLink
