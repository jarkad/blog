{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}

module Example.Routes
  ( Routes (..)
  ) where

import Redirect
import Util

import Servant
import Servant.API.Generic
import Servant.HTML.Blaze

data Routes route = Routes
  { begin    :: route :- "begin"
                      :> UVerb 'GET '[PlainText] '[Redirect 302]
  , callback :: route :- "callback"
                      :> QueryParam' '[Required, Strict] "code" Text
                      :> QueryParam' '[Required, Strict] "state" Text
                      :> Get '[HTML] Html
 } deriving (Generic)
