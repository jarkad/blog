{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}

module Example.Handlers.Begin (handle) where

import App
import Redirect
import Routes (links)

import Network.URI (relativeTo)
import Network.URI.Static (relativeReference)
import Servant

handle :: AppM (Union '[Redirect 302])
handle = do
      conf <- ask
      respond $ redirectLink @302 conf $
        (links ^. #login)
          (conf ^. #user_domain)
          "client_id"
          ([relativeReference|example/callback|] `relativeTo` (conf ^. #app_url))
          "state"
          "code"
          False
