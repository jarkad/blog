{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}

module Example.Handlers.Callback (handle) where

import App
import HTML
import Link
import Routes (links, Routes (..))
import Uri
import Util

import Network.URI (relativeTo)
import Network.URI.Static (relativeReference)
import Text.Blaze.Html5 ((!))
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A

handle :: Text -> Text -> AppM Html
handle code' state' = do
  conf <- ask
  pure $ page "Callback" do
    H.h1 "Callback"
    H.p "code is: "
    H.textarea ! A.readonly "" ! A.cols "60" ! A.rows "10" $
      H.toMarkup code'
    H.p "state is: "
    H.textarea ! A.readonly "" ! A.cols "60" ! A.rows "10" $
      H.toMarkup state'
    H.form ! A.action (example links ^. #begin . to (renderLink conf) . to H.textValue) $ do
      H.button "Replay"
    H.form ! A.action (H.textValue (links^. #validate.to (renderLink conf))) ! A.method "POST" $ do
      H.input ! A.type_ "hidden" ! A.name "code" ! A.value (H.textValue code')
      H.input ! A.type_ "hidden" ! A.name "client_id" ! A.value "client_id"
      H.input ! A.type_ "hidden" ! A.name "redirect_uri" ! A.value (H.textValue (renderURI ([relativeReference|example/callback|] `relativeTo` (conf ^. #app_url))))

      H.button "Validate"

