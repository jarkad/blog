{-# LANGUAGE DeriveGeneric #-}

module CSRF
  ( CSRFToken
  , makeCSRF
  ) where

import Config
import Signed
import Uri
import Util

data CSRFTokenData = CSRFTokenData
  { _client_id :: Text
  , _redirect_uri :: URI
  , _session_state :: Text
  } deriving (Eq, Show, Generic)

instance FromJSON CSRFTokenData
instance ToJSON CSRFTokenData

instance FromHttpApiData CSRFTokenData where
  parseUrlPiece = fromJson

instance ToHttpApiData CSRFTokenData where
  toUrlPiece = toJson

type CSRFToken = Signed CSRFTokenData

makeCSRF :: Config -> Text -> URI -> Text -> CSRFToken
makeCSRF conf client_id redirect_uri session_state =
  sign conf $ CSRFTokenData
    { _client_id = client_id
    , _redirect_uri = redirect_uri
    , _session_state = session_state
    }
