{-# LANGUAGE OverloadedLabels #-}

module Link
  ( Link
  , renderLink
  , linkToURI
  ) where

import Config
import Uri
import Util ()

import Network.URI (relativeTo)
import Servant (Link, linkURI)

renderLink :: Config -> Link -> Text
renderLink conf = renderURI . linkToURI conf

linkToURI :: Config -> Link -> URI
linkToURI conf link = linkURI link `relativeTo` (conf^. #app_url)
