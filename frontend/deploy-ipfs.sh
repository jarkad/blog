set -euo pipefail

DEST_DIR="$1"
shift

dir_hash="$(ipfs add -q -H -r --cid-version 1 "$DEST_DIR" | tail -1)"
printf '/ipfs/%s\n' "$dir_hash"
