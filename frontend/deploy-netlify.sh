set -euo pipefail

AUTH_TOKEN="$1"
shift

SITE_ID="$1"
shift

DESTDIR="$1"
shift

api() {
  local api_endpoint="$1"
  shift
  curl -fsSL -H 'User-Agent: Deployer (jarkad@tuta.io)' -H "Authorization: Bearer $AUTH_TOKEN" "$@" "https://api.netlify.com/api/v1/$api_endpoint"
}

cd "$DESTDIR"

hashes="$(find . -type f | xargs sha1sum | awk '{print substr($2, 3) " " $1}')"

digest="$(printf '%s' "$hashes" | sed -e 's/^/--arg /' | xargs jq -n '{files: $ARGS.named}')"

result="$(printf '%s' "$digest" | api "sites/$SITE_ID/deploys" --json '@-')"
deploy_id="$(printf '%s' "$result" | jq -r '.id')"
required="$(printf '%s' "$result" | jq -r '.required[]')"

printf 'Deploy %s\n' "$deploy_id"

printf 'Required files:\n%s\n' "$required"

printf '%s\n' "$hashes" | while read -r file hash
do
  printf '%s' "$required" | grep "$hash" >/dev/null || continue
  printf 'Uploading %s\n' "$file"
  api "deploys/$deploy_id/files/$file" -T "$file"
done
